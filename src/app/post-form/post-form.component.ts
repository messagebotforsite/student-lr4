import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { FilterType, Post } from '../app.component';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent {
  @ViewChild('myInputText')
  myinputText: ElementRef;
  @ViewChild('myInputTitle') 
  myinputTitle: ElementRef;
  
  @Output() 
  addPostUser: EventEmitter<Post> = new EventEmitter<Post>();

  @Output() 
  titleSearch: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  filterChanged: EventEmitter<FilterType> = new EventEmitter<FilterType>();

  titleSearching = '';
  styleToggle = false;
  title = '';
  text = '';
  
  addPost() {
    if(this.text.trim() && this.title.trim()) {
      const post: Post ={
        title: this.title,
        text: this.text
      }

      this.addPostUser.emit(post);

      this.text = '';
      this.title = '';
    }
  }

  onChangeSearch(change: string) {
    this.titleSearch.emit(change)
  }

  onChangeFilter(event: FilterType) {
    this.filterChanged.emit(event);
  }

  onLoadDefault() {
    this.styleToggle = !this.styleToggle

    if(this.styleToggle) {
      this.myinputText.nativeElement.style.color = 'red';
      this.myinputTitle.nativeElement.style.fontWeight = 'bold';
    } else {
      this.myinputText.nativeElement.style.color = 'black';
      this.myinputTitle.nativeElement.style.fontWeight = 'normal';
    }
  }
}
