import { Pipe, PipeTransform } from '@angular/core';
import { FilterType, Post } from '../app.component';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(post: Post[], titleSearch: string, filterType: FilterType): Post[] {
    if (!titleSearch.trim()) {
      return post;
    }

    return post.filter(item => {
      return item[filterType].toLowerCase().includes(titleSearch.toLowerCase());
    });
  }
}
